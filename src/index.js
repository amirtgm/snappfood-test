import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import ApiClient from './helpers/ApiClient';
import createStore from './redux/store';
import App from './containers/App';
import 'font-awesome/css/font-awesome.min.css';
import './styles/main.scss';

const client = new ApiClient();
const root = document.getElementById('root');
const store = createStore(client);

ReactDOM.render(
  <Provider store={store} key="provider">
    <App />
  </Provider>,
  root,
);
