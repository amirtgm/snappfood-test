export default function clientMiddleware(client) {
  const tempClient = client;
  return ({ dispatch, getState }) => (next) => (action) => {
    if (typeof action === 'function') {
      return action(dispatch, getState);
    }

    const { promise, types, ...rest } = action;
    if (!promise) {
      return next(action);
    }
    const REQUEST = types.toUpperCase();
    const SUCCESS = `${REQUEST}_SUCCESS`;
    const FAILURE = `${REQUEST}_FAILURE`;

    next({
      ...rest,
      load: {
        loading: true,
        loaded: false,
      },
      type: REQUEST,
    });

    const actionPromise = promise(tempClient);
    const load = {
      loading: false,
      loaded: true,
    };
    actionPromise
      .then(
        (result) => next({
          ...rest,
          load,
          result,
          type: SUCCESS,
        }),
        (error) => next({
          ...rest,
          load,
          error,
          type: FAILURE,
        }),
      )
      .catch((error) => {
        next({ ...rest, error, type: FAILURE });
      });

    return actionPromise;
  };
}
