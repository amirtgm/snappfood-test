import superagent from 'superagent';

const methods = [
  'get',
  'post',
  'put',
  'patch',
  'del',
  'head',
];

const formatUrl = (path) => `https://snappfood.ir/mobile/v2/${path}`;

export default class ApiClient {
  constructor() {
    methods.forEach((method) => {
      this[method] = (path, { params } = {}) => {
        const promise = new Promise((resolve, reject) => {
          const request = superagent[method](formatUrl(path));
          if (params) {
            request.query(params);
          }
          request.end((err, { body, status } = {}) => {
            const tempBody = body;
            if (tempBody && typeof tempBody === 'object') {
              tempBody.statusCode = status;
            }
            if (err) {
              reject(tempBody || err);
            } else {
              resolve(tempBody);
            }
          });
        });
        return promise;
      };
    });
  }
}
