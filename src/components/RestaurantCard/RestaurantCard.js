import React from 'react';
import './restaurantCard.scss';

function RestaurantCard({
  title,
  address,
  rate,
  cuisines,
  tags,
  reviewCounts,
  logo,
}) {
  return (
    <div className="restaurant-card">
      <div className="restaurant-inner-info">
        <img
          src={logo}
          alt={title}
        />
        <div className="restaurant-content">
          <h4 className="restaurant-title">{title}</h4>
          {cuisines && <span className="restaurant-cuisines">{cuisines.map((i) => i.title).join('٬')}</span>}
          <span className="restaurant-address">
            {address}
          </span>
        </div>
      </div>
      <div className="restaurant-additional-info">
        <div className="restaurant-tags-wrapper">
          <span className="restaurant-tags">اکسپرس</span>
          <span className="restaurant-tags">اکسپرس</span>
          <span className="restaurant-tags">اکسپرس</span>
        </div>
        <div className="restaurant-rate-wrapper">
          <span>
            {reviewCounts}
نظر
          </span>
          <span className="restaurant-rate">{rate}</span>
        </div>
      </div>
    </div>
  );
}

export default RestaurantCard;
