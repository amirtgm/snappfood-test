import React, { useState } from 'react';
import './searchBar.scss';
// import { ReactComponent as searchIcon } from './search-icon.svg';

function SearchBar({ setSearch }) {
  const [inputeValue, setInputValue] = useState('');
  return (
    <div className="search-bar">
      <input type="text" value={inputeValue} onChange={(e) => setInputValue(e.target.value)} placeholder="جستجو در غذا، رستوران و..." />
      <span className="search-icon" onClick={() => setSearch(inputeValue)}>
        <i className="fa fa-search" />
      </span>
    </div>
  );
}

export default SearchBar;
