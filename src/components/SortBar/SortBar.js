import React from 'react';
import './sortBar.scss';

function SortBar({ values, setSort, activeSort }) {
  return (
    <div className="sort-bar">
      {Array.isArray(values) && values.map((sortItem) => (
        <span
          key={`${sortItem.value}-${Math.random()}`}
          className={`sort-chips ${activeSort === sortItem.value && 'active-sort'}`}
          onClick={() => setSort(sortItem.value)}
        >
          {sortItem.title}
        </span>
      ))}
    </div>
  );
}

export default SortBar;
