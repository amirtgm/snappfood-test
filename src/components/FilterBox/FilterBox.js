import React, { useState } from 'react';
import SearchBar from '../SearchBar/SearchBar';
import SortBar from '../SortBar/SortBar';
import './filterBox.scss';

function FilterBox({ filters, setActiveFilter }) {
  const setSearch = (q) => {
    setActiveFilter({
      search: q,
    });
  };
  const setSort = (sort) => {
    setActiveFilter({
      sort,
    });
  };

  return (
    <div className="filter-box">
      <SearchBar setSearch={setSearch} />
      <SortBar
        values={Array.isArray(filters) && filters.filter((f) => f.value === 'sortings')[0].restaurantFilters}
        setSort={setSort}
      />
    </div>
  );
}

export default FilterBox;
