import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Restaurant from './Restaurants/Restaurants';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/restaurants">
          <Restaurant />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
