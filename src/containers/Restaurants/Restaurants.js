import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { loadFilters, loadRestaurants, activeFilter } from '../../redux/actions/restaurants.action';
import FilterBox from '../../components/FilterBox/FilterBox';
import RestaurantCard from '../../components/RestaurantCard/RestaurantCard';

function Restaurant({
  data, loadFilters, loadRestaurants, activeFilter, activeFilters,
}) {
  const [hasMore, setHasMore] = useState(false);
  const loadRestaurantsWithFilter = ({ page } = {}) => loadRestaurants({
    filters: {
      sortings: activeFilters.sort ? [activeFilters.sort] : [],
      search: activeFilters.search ? activeFilters.search : '',
    },
    page: page || 0,
    page_size: 20,
  });
  useEffect(() => {
    loadFilters();
    loadRestaurants().then(() => {
      setHasMore(true);
    });
  }, []);
  useEffect(() => {
    loadRestaurantsWithFilter();
  }, [activeFilters.sort, activeFilters.search]);

  const loadMore = (x) => {
    setHasMore(false);
    loadRestaurantsWithFilter({ page: x }).then(() => {
      setHasMore(true);
    });
  };
  return (
    <div className="body">
      <FilterBox filters={data.filters} setActiveFilter={activeFilter} />
      <InfiniteScroll pageStart={0} loadMore={loadMore} hasMore={hasMore} loader={<i className="fa fa-spinner" />}>
        {Array.isArray(data.restaurants)
          && data.restaurants.map(
            (restaurant) => restaurant.type === 'VENDOR' && (
            <RestaurantCard
              title={restaurant.data.title}
              address={restaurant.data.address}
              rate={restaurant.data.rate}
              cuisines={restaurant.data.cuisinesArray}
              tags={restaurant.data.tags}
              reviewCounts={restaurant.data.countReview}
              logo={restaurant.data.logo}
            />
            ),
          )}
      </InfiniteScroll>
    </div>
  );
}
export default connect(
  (state) => ({
    data: state.restaurants,
    activeFilters: state.restaurants.activeFilters,
  }),
  {
    loadFilters,
    loadRestaurants,
    activeFilter,
  },
)(Restaurant);
