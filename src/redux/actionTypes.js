import createconstants from '../utils/createConstants';
import createType from '../utils/actionTypesFormat';

export default createconstants([createType('restaurants'), createType('filters'), createType('active_filter')]);
