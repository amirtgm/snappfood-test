import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import restaurants from './restaurants.reducer';

export default combineReducers({
  routing: routerReducer,
  restaurants,
});
