import ActionTypes from '../actionTypes';

const initialState = {
  loaded: false,
  passed: false,
  filters: {},
  restaurants: {},
  activeFilters: {},
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ActionTypes.LOAD_FILTERS:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.LOAD_FILTERS_SUCCESS:
      return {
        ...state,
        ...action.load,
        filters: action.result.data.restaurantFilterTypes,
      };
    case ActionTypes.LOAD_FILTERS_FAIL:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.LOAD_RESTAURANTS:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.LOAD_RESTAURANTS_SUCCESS:
      return {
        ...state,
        ...action.load,
        restaurants: action.result.data.finalResult,
      };
    case ActionTypes.LOAD_RESTAURANTS_FAIL:
      return {
        ...state,
        ...action.load,
      };
    case ActionTypes.ACTIVE_FILTER:
      console.log(action);
      return {
        ...state,
        activeFilters: {
          ...state.activeFilters,
          ...action.payload,
        },
      };
    default:
      return state;
  }
}
