export function loadFilters() {
  return {
    types: 'load_filters',
    promise: (client) => client.get('restaurant/filters'),
  };
}
export function loadRestaurants(params) {
  return {
    types: 'load_restaurants',
    promise: (client) => client.get('restaurant/vendors-list', {
      params: {
        ...params,
        page: 0,
        page_size: 20,
      },
    }),
  };
}
export function activeFilter(payload) {
  return {
    type: 'ACTIVE_FILTER',
    payload,
  };
}
