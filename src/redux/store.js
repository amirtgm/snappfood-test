import { createStore as _createStore, applyMiddleware, compose } from 'redux';
import createMiddleware from '../helpers/clientMiddleware';
import reducer from './reducers';

export default function createStore(client, data) {
  const middleware = [createMiddleware(client)];
  const finalCreateStore = compose(applyMiddleware(...middleware))(_createStore);
  const store = finalCreateStore(reducer, data);
  return store;
}
